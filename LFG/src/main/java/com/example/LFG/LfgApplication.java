package com.example.LFG;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class LfgApplication {

	public static void main(String[] args) {
		SpringApplication.run(LfgApplication.class, args);
	}

}
